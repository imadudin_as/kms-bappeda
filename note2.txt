
Kategori id 
  belongsTo Forum

Forum id user_id, kategori_id
  hasMany Kategori
  belongsTo User
  hasMany Reply

Reply id user_id post_id
  belongsTo Forum
  belongsTo User
  
User id
  hasMany Replies
  hasMany Forum

====================================================================================

status id
belongsTo Dokumen
belongsTo Request

jabatan id
belongsTo User

bidang id
belongsTo User

User id jabatan_id bidang_id
hasMany jabatan
hasMany bidang 
hasMany Dokumen
hasMany Request
hasMany Komentar

Request id user_id
belongsTo User
hasMany Komentar
hasMany Status

Dokumen id user_id
belongsTo User
hasMany Komentar
hasMany Status

Komentar id user_id request_id document_id
belongsTo user
belongsTo request
belongsTo dokumen

====================================================================================

Status
- Belum divalidasi
- Valid
- Tidak Valid
- Buka
- Tutup