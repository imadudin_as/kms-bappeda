<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBidangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bidangs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama');
            $table->timestamps();
        });

        DB::table('bidangs')->insert([
            ['nama' => 'Kepala Badan'],
            ['nama' => 'Sekretaris'],
            ['nama' => 'Program'],
            ['nama' => 'Keuangan dan Aset'],
            ['nama' => 'Umum dan Kepegawaian'],
            ['nama' => 'Perencanaan dan Perekonomian'],
            ['nama' => 'Pertanian'],
            ['nama' => 'Pariwisata, Industri dan Perdagangan'],
            ['nama' => 'Ekonomi, Keuangan dan Pembiayaan'],
            ['nama' => 'Perencanaan Makro dan Evaluasi'],
            ['nama' => 'Makro'],
            ['nama' => 'Pengendalian Kebijakan'],
            ['nama' => 'Monitoring dan Evaluasi'],
            ['nama' => 'Perencanaan Infrastruktur dan Kewilayahan'],
            ['nama' => 'Infrastruktur'],
            ['nama' => 'Tata Ruang dan Permukiman'],
            ['nama' => 'Sumber Daya Alam dan Lingkungan'],
            ['nama' => 'Perencanaan Pemerintahaan dan Pembangunan Manusia'],
            ['nama' => 'Pemerintahan'],
            ['nama' => 'Sumber Daya Manusia'],
            ['nama' => 'Kesejahteraan Rakyat'],
            ['nama' => 'Pusat Data dan Informasi Pembangunan Daerah'],
            ['nama' => 'Tata Usaha'],
            ['nama' => 'Data Geo Spasial'],
            ['nama' => 'Data Statistik'],
            ['nama' => 'Kelompok Jabatan Fungsional'],
            ]);
        }
        
        /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bidangs');
    }
}
