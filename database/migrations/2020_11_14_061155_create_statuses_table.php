<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statuses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama');
            $table->timestamps();
        });

        DB::table('statuses')->insert([
            ['nama' => 'Belum Divalidasi'],
            ['nama' => 'Valid'],
            ['nama' => 'Tidak Valid'],
            ['nama' => 'Terbuka'],
            ['nama' => 'Tertutup'],
            ['nama' => 'On Progress'],
            ['nama' => 'Selesai'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('statuses');
    }
}
