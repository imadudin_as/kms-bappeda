<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJabatansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jabatans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama');
            $table->timestamps();
        });

        DB::table('jabatans')->insert([
            ['nama' => 'Kepala Badan'],
            ['nama' => 'Sekretaris'],
            ['nama' => 'Kepala Sub Bagian'],
            ['nama' => 'Kepala Bidang'],
            ['nama' => 'Kepala Sub Bidang'],
            ['nama' => 'Kepala Seksi'],
            ['nama' => 'Staff'],
            ['nama' => 'Kelompok Jabatan Fungsional'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jabatans');
    }
}
