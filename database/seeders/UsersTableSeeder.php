<?php
namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'email' => 'kepalauptd@mail.com',
            'nama' => 'Kepala UPTD',
            'NIP' => '199001171998031002',
            'jenis_kelamin' => 'Laki-Laki',
            'tempat_lahir' => 'Jakarta',
            'tgl_lahir' => '1990-01-17',
            'no_hp' => '088888888888',
            'level_id' => '2',
            'jabatan_id' => '4',
            'bidang_id' => '22',
            'email_verified_at' => now(),
            'password' => Hash::make('12341234'),
            'foto' => 'no_avatar.png',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('users')->insert([
            'email' => 'admin@mail.com',
            'nama' => 'Administrator',
            'NIP' => '199001171998031002',
            'jenis_kelamin' => 'Laki-Laki',
            'tempat_lahir' => 'Jakarta',
            'tgl_lahir' => '1990-01-17',
            'no_hp' => '088888888888',
            'level_id' => '1',
            'jabatan_id' => '4',
            'bidang_id' => '22',
            'email_verified_at' => now(),
            'password' => Hash::make('12341234'),
            'foto' => 'no_avatar.png',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        DB::table('users')->insert([
            'email' => 'karyawan@mail.com',
            'nama' => 'Karyawan',
            'NIP' => '199001171998031002',
            'jenis_kelamin' => 'Laki-Laki',
            'tempat_lahir' => 'Jakarta',
            'tgl_lahir' => '1990-01-17',
            'no_hp' => '088888888888',
            'level_id' => '3',
            'jabatan_id' => '4',
            'bidang_id' => '22',
            'email_verified_at' => now(),
            'password' => Hash::make('12341234'),
            'foto' => 'no_avatar.png',
            'created_at' => now(),
            'updated_at' => now()
        ]);
    }
}

