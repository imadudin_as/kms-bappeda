<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Auth;
use App\Models\RequestFile;
use App\Models\Status;
use App\Models\RequestKomentar;

class RequestsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  
        $reqs = RequestFile::where('status_id', 6)->orderBy('created_at', 'desc')->paginate(10);
        return view('requests.index')->with('reqs', $reqs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('requests.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
            'deskripsi' => 'required',
            'jenis_file' => 'required'
        ]);

        $req = new RequestFile;
        $req->nama = $request->input('nama');
        $req->deskripsi = $request->input('deskripsi');
        $req->jenis_file = $request->input('jenis_file');
        $req->tgl_req = Carbon::now();
        $req->status_id = 6;
        $req->user_id = Auth::user()->id;
        $req->save();

        return redirect('/request')->with('success', 'Request Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $req = RequestFile::find($id);
        return view('requests.show')->with('req', $req);
    }

    public function selesai(Request $request)
    {
        $req = RequestFile::find($request->id);
        $req->status_id = $request->status_id;
        $req->save();

        return redirect('/request')->with('success', 'Request Telah Selesai');
    }

    public function req_comment_create(Request $request)
    {
        $req = RequestFile::where('id','=',$request->get('id'))->first();
        if($req)
        {
            $this->validate($request, [
                'isi' => 'required',
            ]);

            RequestKomentar::create([
                'user_id' => Auth::user()->id,
                'request_file_id' => $req->id,
                'isi' => $request->input('isi')
            ]);

            return redirect()->back();
        }

        return redirect('/');
    }

    public function req_comment_edit($id)
    {
        $komentar = RequestKomentar::findOrFail($id);
        return view('requests.show',compact('komentar'));
    }

    public function req_comment_update(Request $request, $id)
    {
        $this->validate($request,['isi' => 'required']);
        $komentar = RequestKomentar::findOrFail($id);
        $komentar->update($request->all());
        return redirect()->back();
    }

    public function req_comment_delete($id)
    {
        $komentar = RequestKomentar::findOrFail($id);
        $komentar->delete();
        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $req = RequestFile::find($id);

        if(auth()->user()->level_id == 1 || auth()->user()->level_id == 2) {
            return view('requests.edit')->with('req', $req);
        }

        if(auth()->user()->id !== $req->user_id ){
            return redirect('/request')->with('error', 'Unauthorized Action');
        }
        
        return view('requests.edit')->with('req', $req);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama' => 'required',
            'deskripsi' => 'required',
            'jenis_file' => 'required'
        ]);

        $req = RequestFile::find($id);
        $req->nama = $request->input('nama');
        $req->deskripsi = $request->input('deskripsi');
        $req->jenis_file = $request->input('jenis_file');
        $req->status_id = 6;
        $req->save();

        return redirect('/request/'.$req->id)->with('success', 'Request Berhasil Diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $req = RequestFile::find($id);

        if(auth()->user()->level_id == 1 || auth()->user()->level_id == 2) {
            $req->delete();
            return redirect('/request')->with('success', 'Request Berhasil Dihapus');
        }

        if(auth()->user()->id !== $req->user_id ){
            return redirect('/request')->with('error', 'Unauthorized Action');
        }

        $req->delete();
        return redirect('/request')->with('success', 'Request Berhasil Dihapus');
    }
}
