<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function document() {
        $title = 'Dokumen';
        return view('documents.index')->with('title', $title);
    }

    public function request() {
        return view('requests.index');
    }

    public function forum_diskusi() {
        return view('forums.index');
    }

    public function index() {
        return view('index');
    }
    
    public function manage_user() {
        return view('users.index');
    }

    public function validasi_dokumen() {
        $data = array(
            'title' => 'validasi dokumen',
            'status' => ['Belum Divalidasi', 'Valid', 'Tidak Valid']
        );
        return view('validations.index')->with($data);
    }

}
