<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Document;
use App\Models\Status;
use Carbon\Carbon;
use Auth;
use DB;

class ValidationsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->user()->level_id >= 3){
            return redirect('/index')->with('error', 'Unauthorized Action');
        }
        
        $docs = Document::where('status_id', 1)->get();
        return view('validations.index', compact('docs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(auth()->user()->level_id >= 3){
            return redirect('/index')->with('error', 'Unauthorized Action');
        }

        $statuses = Status::whereIn('id', [2, 3])->get();
        $doc = Document::find($id);
        return view('validations.edit', compact('doc', 'statuses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'status_id' => 'required'
        ]);

        $doc = Document::find($id);
        $doc->status_id = $request->input('status_id');
        $doc->save();

        return redirect('/validasi')->with('success', 'Dokumen Berhasil Divalidasi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
