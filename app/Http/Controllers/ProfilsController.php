<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\Models\User;
use Carbon\Carbon;
use App\Models\Jabatan;
use App\Models\Bidang;

class ProfilsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'show']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $profil = User::find($id);
        return view('profils.show', compact('profil'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profil = User::find($id);
        if(auth()->user()->id !== $profil->id){
            return redirect('/profil/'.$profil->id)->with('error', 'Unauthorized Action');
        }
        return view('profils.edit', compact('profil'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama' => 'required',
            'jenis_kelamin' => 'required',
        ]);

        // Handle File Upload
        if($request->hasFile('foto')){
            // Get filename with the extension
            $fileNameWithExt = $request->file('foto')->getClientOriginalName();
            // Get just filename
            $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            // Get just extension
            $extension = $request->file('foto')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore = $fileName.'_'.time().'.'.$extension;
            // Upload image
            $path = $request->file('foto')->storeAs('public/foto', $fileNameToStore);
        }

        $profil = User::find($id);
        $profil->nama = $request->input('nama');
        $profil->jenis_kelamin = $request->input('jenis_kelamin');
        $profil->tempat_lahir = $request->input('tempat_lahir');
        $profil->tgl_lahir = $request->input('tgl_lahir');
        $profil->no_hp = $request->input('no_hp');
        $profil->password = Hash::make($request->input('password'));
        if($request->hasFile('foto')){
            // Delete image
            if($profil->foto != 'no_avatar.png'){
                Storage::delete('public/foto/'.$profil->foto);
            }

            $profil->foto = $fileNameToStore;
        }
        $profil->save();

        return redirect('/profil/'.$profil->id)->with('success', 'Profil Berhasil Diedit');
    }

}
