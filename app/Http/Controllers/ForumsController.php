<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Auth;
use App\Models\Forum;
use App\Models\Diskusi;

class ForumsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $forums = Forum::orderBy('created_at', 'desc')->paginate(10);
        return view('forums.index', compact('forums'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('forums.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'judul' => 'required',
            'deskripsi' => 'required'
        ]);

        $forums = new Forum;
        $forums->judul = $request->input('judul');
        $forums->deskripsi = $request->input('deskripsi');
        $forums->tgl_forum = Carbon::now();
        $forums->user_id = Auth::user()->id;
        $forums->status_id = 4;
        $forums->save();

        return redirect('/forum')->with('success', 'Forum Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $forum = Forum::find($id);
        return view('forums.show', compact('forum'));
    }

    public function tutup(Request $request)
    {
        $forum = Forum::find($request->id);
        $forum->status_id = $request->status_id;
        $forum->save();

        return redirect('/forum')->with('success', 'Forum Telah Ditutup');
    }

    public function buka(Request $request)
    {
        $forum = Forum::find($request->id);
        $forum->status_id = $request->status_id;
        $forum->save();

        return redirect('/forum')->with('success', 'Forum Telah Dibuka');
    }

    public function diskusi_create(Request $request)
    {
        $forum = Forum::where('id', '=',$request->get('id'))->first();
        if($forum)
        {
            $this->validate($request, [
                'isi' => 'required',
            ]);

            Diskusi::create([
                'user_id' => Auth::user()->id,
                'forum_id' => $forum->id,
                'isi' => $request->input('isi')
            ]);

            return redirect()->back();
        }

        return redirect('/');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $forum = Forum::find($id);

        if(auth()->user()->level_id == 1 || auth()->user()->level_id == 2) {
            return view('forums.edit', compact('forum'));
        }

        if(auth()->user()->id !== $forum->user_id ){
            return redirect('/forum')->with('error', 'Unauthorized Action');
        }
        return view('forums.edit', compact('forum'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'judul' => 'required',
            'deskripsi' => 'required'
        ]);

        $forums = Forum::find($id);
        $forums->judul = $request->input('judul');
        $forums->deskripsi = $request->input('deskripsi');
        $forums->status_id = 4;
        $forums->save();

        return redirect('/forum/'.$forums->id)->with('success', 'Forum Berhasil Diedit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $forum = Forum::find($id);

        if(auth()->user()->level_id == 1 || auth()->user()->level_id == 2) {
            $forum->delete();

            return redirect('/forum')->with('success', 'Forum Berhasil Dihapus');
        }

        if(auth()->user()->id !== $forum->user_id ){
            return redirect('/forum')->with('error', 'Unauthorized Action');
        }
        $forum->delete();

        return redirect('/forum')->with('success', 'Forum Berhasil Dihapus');
    }
}
