<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Auth;
use App\Models\User;
use App\Models\Jabatan;
use App\Models\Bidang;
use App\Models\Level;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->user()->level_id !== 1 ){
            return redirect('/index')->with('error', 'Unauthorized Action');
        }

        $users = User::all();
        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(auth()->user()->level_id !== 1 ){
            return redirect('/index')->with('error', 'Unauthorized Action');
        }

        $jabatans = Jabatan::all();
        $bidangs = Bidang::all();
        $levels = Level::all();
        return view('users.create', compact('jabatans', 'bidangs', 'levels'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request, [
            'nama' => 'required',
            'NIP' => 'required',
            'jenis_kelamin' => 'required',
            'email' => 'required',
            'password' => 'required',
        ]);

        $user = new User;
        $user->nama = $request->input('nama');
        $user->NIP = $request->input('NIP');
        $user->jenis_kelamin = $request->input('jenis_kelamin');
        $user->tempat_lahir = 'null';
        $user->tgl_lahir = '2000-01-01';
        $user->no_hp = '000000000000';
        $user->bidang_id = $request->input('bidang_id');
        $user->jabatan_id = $request->input('jabatan_id');
        $user->level_id = $request->input('level_id');
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password'));
        $user->foto = 'no_avatar.png';
        $user->save();

        return redirect('/users')->with('success', 'User Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(auth()->user()->level_id !== 1 ){
            return redirect('/index')->with('error', 'Unauthorized Action');
        }

        $user = User::find($id);
        return view('users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(auth()->user()->level_id !== 1 ){
            return redirect('/index')->with('error', 'Unauthorized Action');
        }

        $jabatans = Jabatan::all();
        $bidangs = Bidang::all();
        $levels = Level::all();
        $user = User::find($id);

        
        return view('users.edit', compact('user', 'jabatans', 'bidangs', 'levels'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama' => 'required',
            'NIP' => 'required',
            'jenis_kelamin' => 'required',
            'email' => 'required',
        ]);

        $user = User::find($id);
        $user->nama = $request->input('nama');
        $user->NIP = $request->input('NIP');
        $user->jenis_kelamin = $request->input('jenis_kelamin');
        $user->bidang_id = $request->input('bidang_id');
        $user->jabatan_id = $request->input('jabatan_id');
        $user->level_id = $request->input('level_id');
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password'));
        $user->save();

        return redirect('/users')->with('success', 'User Berhasil Diedit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(auth()->user()->level_id !== 1 ){
            return redirect('/index')->with('error', 'Unauthorized Action');
        }

        $user = User::find($id);
        $user->delete();

        return redirect('/users')->with('success', 'User Berhasil Dihapus');
    }
}
