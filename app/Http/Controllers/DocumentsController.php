<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Auth;
use App\Models\Document;
use App\Models\Status;
use App\Models\DokumenKomentar;

class DocumentsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show', 'search']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $docs = Document::where('status_id', 2)->orderBy('created_at', 'desc')->paginate(10);
        return view('documents.index', compact('docs'));
    }

    /**
     * Display a search result of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search()
    {
        $search = $_GET['cari'];

        $docs = Document::where('status_id', 2)->where("nama","like","%{$search}%")
                ->orWhere('status_id', 2)->where("jenis_file","like","%{$search}%")
                ->orWhere('status_id', 2)->where("tahun","like","%{$search}%")
                ->get();
        return view('documents.search', compact('docs', 'search'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $status = Status::where('id', '<=', '3')->get();
        return view('documents.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
            'file' => 'required|max:25000',
            'deskripsi' => 'required',
            'halaman' => 'required',
            'jenis_file' => 'required',
            'tahun' => 'required',
        ]);

        // Handle File Upload
        if($request->hasFile('file')){
            // Get filename with the extension
            $fileNameWithExt = $request->file('file')->getClientOriginalName();
            // Get just filename
            $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            // Get just extension
            $extension = $request->file('file')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore = $fileName.'_'.time().'.'.$extension;
            // Upload image
            $path = $request->file('file')->storeAs('public/file', $fileNameToStore);
        }

        $docs = new Document;
        $docs->nama = $request->input('nama');
        $docs->deskripsi = $request->input('deskripsi');
        $docs->file = $fileNameToStore;
        $docs->tgl_upload = Carbon::now();
        $docs->halaman = $request->input('halaman');
        $docs->tahun = $request->input('tahun');
        $docs->jenis_file = $request->input('jenis_file');
        $docs->user_id = Auth::user()->id;
        $docs->status_id = 1;
        $docs->save();

        return redirect('/docs')->with('success', 'Dokumen Berhasil Ditambahkan, Menunggu Validasi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $doc = Document::with('komentars')->find($id);
        // print_r($doc->komentars->count());
        return view('documents.show')->with('doc', $doc);
    }

    // Buat Komentar
    public function comment_create(Request $request)
    {
        $doc = Document::where('id','=',$request->get('id'))->first();
        if($doc)
        {
            $this->validate($request, [
                'isi' => 'required',
            ]);

            DokumenKomentar::create([
                'user_id' => Auth::user()->id,
                'document_id' => $doc->id,
                'isi' => $request->input('isi')
            ]);

            return redirect()->back();
        }

        return redirect('/');
    }

    public function comment_edit($id)
    {
        $komentar = DokumenKomentar::findOrFail($id);
        return view('documents.show',compact('komentar'));
    }

    public function comment_update(Request $request, $id)
    {
        $this->validate($request,['isi' => 'required']);
        $komentar = DokumenKomentar::findOrFail($id);
        $komentar->update($request->all());
        return redirect()->back();
    }

    public function comment_delete($id)
    {
        $komentar = DokumenKomentar::findOrFail($id);
        $komentar->delete();
        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $doc = Document::find($id);

        if(auth()->user()->level_id == 1 || auth()->user()->level_id == 2) {
            return view('documents.edit', compact('doc'));
        }

        if(auth()->user()->id !== $doc->user_id ){
            return redirect('/docs')->with('error', 'Unauthorized Action');
        }

        return view('documents.edit', compact('doc'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama' => 'required',
            'file' => 'max:25000',
            'deskripsi' => 'required',
            'halaman' => 'required',
            'jenis_file' => 'required',
        ]);

        // Handle File Upload
        if($request->hasFile('file')){
            // Get filename with the extension
            $fileNameWithExt = $request->file('file')->getClientOriginalName();
            // Get just filename
            $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            // Get just extension
            $extension = $request->file('file')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore = $fileName.'_'.time().'.'.$extension;
            // Upload image
            $path = $request->file('file')->storeAs('public/file', $fileNameToStore);
        }

        $docs = Document::find($id);
        $docs->nama = $request->input('nama');
        $docs->deskripsi = $request->input('deskripsi');
        if($request->hasFile('file')){
            // Delete image
            Storage::delete('public/file/'.$docs->file);

            $docs->file = $fileNameToStore;
        }
        $docs->halaman = $request->input('halaman');
        $docs->tahun = $request->input('tahun');
        $docs->jenis_file = $request->input('jenis_file');
        $docs->status_id = 1;
        $docs->save();

        return redirect('/docs/'.$docs->id)->with('success', 'Dokumen Berhasil Diubah, Menunggu Validasi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $doc = Document::find($id);

        if(auth()->user()->level_id == 1 || auth()->user()->level_id == 2) {
            $doc->delete();

            return redirect('/docs')->with('success', 'Dokumen Berhasil Dihapus');
        }

        if(auth()->user()->id !== $doc->user_id ){
            return redirect('/docs')->with('error', 'Unauthorized Action');
        }
        $doc->delete();

        return redirect('/docs')->with('success', 'Dokumen Berhasil Dihapus');
    }
}
