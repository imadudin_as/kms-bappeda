<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RequestFile;

class HomeController extends Controller
{
    

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $reqs = RequestFile::where('status_id', 6)->orderBy('created_at', 'desc')->take(3)->get();
        return view('index', compact('reqs'));
    }
}
