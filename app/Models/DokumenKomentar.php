<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DokumenKomentar extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'document_id',
        'isi'
    ];

    public function user() {
        return $this->belongsTo('App\Models\User');
    }

    public function document() {
        return $this->belongsTo('App\Models\Document');
    }

    public function users() {
        return $this->belongsTo('App\Models\DokumenKomentar');
    }
}
