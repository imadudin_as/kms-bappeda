<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Forum extends Model
{
    use HasFactory;

    protected $fillable = [
        'judul',
        'deskripsi',
        'tgl_forum',
        'user_id',
        'status_id',

    ];

    public function user() {
        return $this->belongsTo('App\Models\User');
    }

    public function statuses() {
        return $this->hasMany('App\Models\Status');
    }

    public function diskusis() {
        return $this->hasMany('App\Models\Diskusi');
    }
}
