<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RequestKomentar extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'request_file_id',
        'isi'
    ];

    public function user() {
        return $this->belongsTo('App\Models\User');
    }

    public function request() {
        return $this->belongsTo('App\Models\RequestFile');
    }

    public function komen() {
        return $this->belongsTo('App\Models\RequestKomentar');
    }
}
