<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Diskusi extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'forum_id',
        'isi'
    ];

    public function user() {
        return $this->belongsTo('App\Models\User');
    }

    public function forum() {
        return $this->belongsTo('App\Models\Forum');
    }

    public function diskusi() {
        return $this->belongsTo('App\Models\Diskusi');
    }
}
