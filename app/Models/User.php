<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'nama',
        'NIP',
        'jenis_kelamin',
        'tempat_lahir',
        'tgl_lahir',
        'no_hp',
        'level',
        'jabatan_id',
        'bidang_id',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function jabatans() {
        return $this->hasMany('App\Models\Jabatan');
    }

    public function bidangs() {
        return $this->hasMany('App\Models\Bidang');
    }

    public function levels() {
        return $this->hasMany('App\Models\Level');
    }

    public function documents() {
        return $this->hasMany('App\Models\Document');
    }

    public function requests() {
        return $this->hasMany('App\Models\RequestFile');
    }

    public function doc_comments() {
        return $this->hasMany('App\Models\DokumenKomentar');
    }

    public function req_comments() {
        return $this->hasMany('App\Models\RequestKomentar');
    }

    public function forums() {
        return $this->hasMany('App\Models\Forum');
    }
    
    public function diskusis() {
        return $this->hasMany('App\Models\Diskusi');
    }

    public function bidang() {
        return $this->belongsTo('App\Models\Bidang');
    }

    public function jabatan() {
        return $this->belongsTo('App\Models\Jabatan');
    }
    
}
