<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RequestFile extends Model
{
    use HasFactory;

    protected $table = 'requests';

    protected $fillable = [
        'nama',
        'deskripsi',
        'tgl_req',
        'status_id',
        'user_id',
    ];

    public function statuses() {
        return $this->hasMany('App\Models\Status');
    }

    public function user() {
        return $this->belongsTo('App\Models\User');
    }

    public function status() {
        return $this->belongsTo('App\Models\Status');
    }

    public function req_komentars() {
        return $this->hasMany('App\Models\RequestKomentar');
    }

    public function document() {
        return $this->belongsTo('App\Models\Document');
    }
}
