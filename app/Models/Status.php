<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    use HasFactory;

    protected $fillable = [
        'nama'
    ];

    public function document() {
        return $this->belongsTo('App\Models\Document');
    }

    public function request() {
        return $this->belongsTo('App\Models\RequestFile');
    }

    public function forum() {
        return $this->belongsTo('App\Models\Forum');
    }

}
