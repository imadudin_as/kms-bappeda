<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    use HasFactory;

    protected $fillable = [
        'nama',
        'file',
        'deskripsi',
        'tgl_upload',
        'halaman',
        'tahun',
        'jenis_id',
        'user_id',
        'status_id',
    ];

    public function user() {
        return $this->belongsTo('App\Models\User');
    }

    public function statuses() {
        return $this->hasMany('App\Models\Status');
    }

    public function komentars() {
        return $this->hasMany('App\Models\DokumenKomentar');
    }

    public function komentar() {
        return $this->belongsTo('App\Models\DokumenKomentar');
    }

    public function status() {
        return $this->belongsTo('App\Models\Status');
    }

}
