@extends('layouts.app')


@section('content')
<section class="con-pad h-striped">
    <div class="container">
        <div class="card" style="padding: 1rem 0rem">
            <div class="card-body ">
                <a href="#" onclick="history.go(-1)"><button class="btn btn-secondary"><i class="fa fa-chevron-left" style="padding-right: 15px"></i> Kembali</button></a>
            </div>
            <hr>
            <div class="col-12">
                {!! Form::open(['action' => ['App\Http\Controllers\ProfilsController@update', $profil->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                    <div class="form-group row">
                        <label for="nama" class="col-sm-2 col-form-label">Nama</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama Request..." value="{{$profil->nama}}" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="NIP" class="col-sm-2 col-form-label">NIP</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" name="NIP" id="NIP" placeholder="NIP" value="{{$profil->NIP}}" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="jenis_kelamin" class="col-sm-2 col-form-label">Jenis Kelamin</label>
                        <div class="col-sm-10">
                            <select name="jenis_kelamin" id="jenis_kelamin" class="form-control">
                                <option value="Laki-Laki" @if ($profil->jenis_kelamin == 'Laki-Laki')selected @endif>Laki-Laki</option>
                                <option value="Perempuan" @if ($profil->jenis_kelamin == 'Perempuan')selected @endif>Perempuan</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="jabatan_id" class="col-sm-2 col-form-label">Jabatan</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="jabatan_id" id="jabatan_id" disabled>
                                <option value="{{$profil->jabatan_id}}">{{$profil->jabatan->nama}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="bidang_id" class="col-sm-2 col-form-label">Bidang</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="bidang_id" id="bidang_id" disabled>
                                <option value="{{$profil->bidang_id}}">{{$profil->bidang->nama}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="tempat_lahir" class="col-sm-2 col-form-label">Tempat Lahir</label>
                        <div class="col-sm-10">
                            <input type="tempat_lahir" class="form-control" name="tempat_lahir" id="tempat_lahir" placeholder="Tempat Lahir" value="{{$profil->tempat_lahir}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="tgl_lahir" class="col-sm-2 col-form-label">Tanggal Lahir</label>
                        <div class="col-sm-10">
                            <input type="tgl_lahir" class="form-control" name="tgl_lahir" id="tgl_lahir" placeholder="YYYY-MM-DD" value="{{$profil->tgl_lahir}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="no_hp" class="col-sm-2 col-form-label">No. HP</label>
                        <div class="col-sm-10">
                            <input type="no_hp" class="form-control" name="no_hp" id="no_hp" placeholder="No. HP" value="{{$profil->no_hp}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="email" class="col-sm-2 col-form-label">E-Mail</label>
                        <div class="col-sm-10">
                            <input type="email" class="form-control" name="email" id="email" placeholder="E-Mail..." value="{{$profil->email}}" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="password" class="col-sm-2 col-form-label">Password</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="foto" class="col-sm-2 col-form-label">Upload Foto</label>
                        <div class="col-sm-10">
                            <input type="file" class="form-control-file" id="foto" name="foto">
                        </div>
                    </div>
                    <hr>
                    {{Form::hidden('_method', 'PUT')}}
                    <div class="container" style="display: flex; justify-content: flex-end">
                        {{Form::submit('Simpan', ['class' => 'btn btn-primary', 'style' => 'height: 40px; margin-bottom:20px;'])}}
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</section>
@endsection
