@extends('layouts.app')


@section('content')
<section class="spad set-bg" data-setbg="img/foto-1.jpg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="hero__text">
                    <h5>WELCOME TO</h5>
                    <h2>KMS BAPPEDA</h2>
                </div>
                <div class="footer__newslatter" style="width: 80%; margin: auto;">
                    <form action="{{ action('App\Http\Controllers\DocumentsController@search') }}" method="GET">
                        <input type="text" placeholder="Cari Dokumen..." name="cari">
                        <button type="submit"><span><i class="fa fa-search" style="margin-right: 5px"></i> Cari </span></button>
                    </form>
                </div>
                @if(count($reqs) > 0)
                <div class="hero__text">
                    <h5 style="color: #fff">- - Request Dokumen Terbaru - -</h5>
                </div>
                @foreach ($reqs as $req)
                    <a href="/request/{{$req->id}}" class="list-group-item list-group-item-action">
                        <div class="row">
                            <div class="col-1" style="display: flex; justify-content: center">
                                <img src="{{ asset('img') }}/png/{{$req->jenis_file}}.png" alt="" style="width: 100%">
                            </div>
                            <div class="col-10 align-self-center">
                                {{$req->nama}}
                                <br>
                                <small>Oleh : {{$req->user->nama}}</small>
                            </div>
                        </div>
                    </a>
                @endforeach
                @endif
            </div>
        </div>
    </div>
</section>
@endsection
