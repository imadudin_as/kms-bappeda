@if (count($errors) > 0)
    @foreach ($errors->all() as $error)
        <div class="alert alert-danger container col-12" id="timeout">
            {{$error}}
        </div>
    @endforeach
@endif

@if (session('success'))
    <div class="alert alert-success container col-12" id="timeout">
        {{session('success')}}
    </div>
@endif

@if (session('error'))
    <div class="alert alert-danger container col-12" id="timeout">
        {{session('error')}}
    </div>
@endif

<script>
    setTimeout(function(){
    $('#timeout').hide()
}, 5000)
</script>