<div class="container">
    <div class="footer__content">
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="footer__about">
                    <div class="footer__logo">
                        <a href="/home"><img src="{{ asset('img') }}/logo.png" alt=""></a>
                    </div>
                    <ul>
                        <li>Badan Perencanaan Pembangunan Daerah (BAPPEDA) Provinsi Lampung</li>
                        <li>Jl. Wolter Monginsidi No.223 Teluk Betung - Bandar Lampung Kode Pos 35215</li>
                        <li>Telp: (0721) 485458, 486711</li>
                        <li>Email: pelayananppid@gmail.com</li>
                    </ul>
                    <div class="footer__social">
                        <a href="https://www.facebook.com/BappedaProvLampung"><i class="fa fa-facebook"></i></a>
                        <a href="https://twitter.com/bappeda_lpg"><i class="fa fa-twitter"></i></a>
                        <a href="https://www.youtube.com/channel/UCZMZAzUJh0EDYEU5FfV64eg"><i class="fa fa-youtube-play"></i></a>
                        <a href="https://instagram.com/bappeda_lampung"><i class="fa fa-instagram"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 offset-lg-1 col-md-5 offset-md-1 col-sm-6">
                <div class="footer__widget">
                    <h4>Quick Link</h4>
                    <ul>
                        <li><a href="/index">Home</a></li>
                        <li><a href="/docs">Dokumen</a></li>
                        <li><a href="/request">Request</a></li>
                        <li><a href="/forum">Forum Diskusi</a></li>
                        <li><a href="/users">Manajemen User</a></li>
                        <li><a href="/validasi">Validasi Dokumen</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-4 col-md-8 col-sm-12">
                <div class="footer__newslatter">
                    <h4>Subscribe our Newslatter</h4>
                    <form action="#">
                        <input type="text" placeholder="Your E-mail Address">
                        <button type="submit">Subscribe</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="footer__copyright">
        <div class="row">
            <div class="col-lg-12 col-md-12" style="display: flex; justify-content:center">
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                <div class="footer__copyright__text">
                    <p>Copyright &copy; <script>document.write(new Date().getFullYear());</script> BAPPEDA PROVINSI LAMPUNG</p>
                </div>
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            </div>
        </div>
    </div>
</div>