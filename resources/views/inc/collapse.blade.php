<div class="offcanvas__logo">
    <a href="/index.html"><img src="{{ asset('img') }}/logo.png" alt=""></a>
</div>
<div id="mobile-menu-wrap"></div>
<div class="offcanvas__widget" style="position: absolute; bottom: 0;">
    <ul>
        <li><span class="icon_pin_alt"></span> Jl. Wolter Monginsidi No.223</li>
        <li><span style="padding-right: 20px"></span>Teluk Betung, Bandar Lampung</li>
        <li><span class="icon_phone"></span> (0721) 485458, 486711</li>
    </ul>
</div>
<div class="offcanvas__auth">
    <ul>
        <li>
        @guest
            @if (Route::has('login'))
                <a href="{{ route('login') }}" style="text-decoration: underline; font-size: 16px; color: #E9AD28">Login <span class="arrow_right"></span></a>
            @endif
        @else
                <a href="{{ route('logout') }}" 
                onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> LOGOUT <span class="arrow_right"></span></a>
        @endguest
        </li>
    </ul>
</div>