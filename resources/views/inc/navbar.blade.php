<div class="header__nav__option">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
                <div class="header__logo">
                    <a href="/home"><img src="{{ asset('img') }}/logo.png" alt=""></a>
                </div>
            </div>
            <div class="col-lg-10">
                <div class="header__nav">
                    <nav class="header__menu">
                        <ul class="menu__class">
                            <li  class="{{ (request()->is('index')) ? 'active' : '' }}"><a href="/index">Home</a></li>
                            <li  class="{{ (request()->is('docs')) ? 'active' : '' }}"><a href="/docs">Dokumen</a></li>
                            <li  class="{{ (request()->is('request')) ? 'active' : '' }}"><a href="/request">Request</a></li>
                            <li  class="{{ (request()->is('forum')) ? 'active' : '' }}"><a href="/forum">Forum Diskusi</a></li>
                            @guest
                            @else
                                @if(Auth::user()->level_id < 3)
                                <li  class="{{ (request()->is('users', 'validasi')) ? 'active' : '' }}"><a href="#">Admin</a>
                                    <ul class="dropdown">
                                        @if(Auth::user()->level_id == 1)
                                        <li  class="{{ (request()->is('users')) ? 'active' : '' }}"><a href="/users">Manajemen User</a></li>
                                        @endif
                                        <li  class="{{ (request()->is('validasi')) ? 'active' : '' }}"><a href="/validasi">Validasi</a></li>
                                    </ul>
                                </li>
                                @endif
                            @endguest
                            
                            @guest
                            @if (Route::has('login'))
                            <li>
                                <a href="/login"><button class="btn btn-secondary" style="background-color: #e9ad28; color: #fff">Login</button></a>
                            </li>
                            @endif
                            @else
                            <li  class="{{ (request()->is('edit_profil', 'logout')) ? 'active' : '' }}"><a href="#">{{ Auth::user()->nama }}</a>
                                <ul class="dropdown">
                                    <li  class="{{ (request()->is('edit_profil')) ? 'active' : '' }}"><a href="/profil/{{ Auth::user()->id }}">Edit Profil</a></li>
                                    <li  class="{{ (request()->is('edit_profil')) ? 'active' : '' }}"><a href="/dashboard">Dashboard</a></li>
                                    <li  class="{{ (request()->is('logout')) ? 'active' : '' }}">
                                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                            @csrf
                                        </form>
                                    </li>
                                </ul>
                            </li>
                            @endguest
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        <div class="canvas__open">
            <span class="fa fa-bars"></span>
        </div>
    </div>
</div>