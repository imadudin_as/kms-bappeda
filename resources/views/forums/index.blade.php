@extends('layouts.app')

@section('styles')
    <style>
        .concat div {
            overflow: hidden;
            -ms-text-overflow: ellipsis;
            -o-text-overflow: ellipsis;
            text-overflow: ellipsis;
            white-space: nowrap;
            width: inherit;
            max-width: 100%;
        }

    </style>
@endsection

@section('content')
<section class="con-pad h-striped">
    <div class="container">
        @include('inc.messages')
        @if (!Auth::guest())
        <div style="display: flex; justify-content: flex-end; margin-bottom: 1rem">
            <a href="/forum/create"><button class="btn btn-info"><i class="fa fa-plus" style="padding-right"></i> Buat Forum</button></a>
        </div>
        @endif
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3>Forum</h3>
                    </div>
                </div>
                @if(count($forums) > 0)
                    @foreach ($forums as $forum)
                    <br>
                    <div class="card">
                        @if ($forum->status_id == 4)
                        <div class="card-header bg-dark">
                            <h5 style="color:#fff">
                                    <span>Open</span>
                                <span style="margin: 0px 20px">|</span>
                                <span><i class="fa fa-calendar" style="padding-right: 8px"></i> {{ Carbon\Carbon::parse($forum->created_at)->format('d F Y') }}</span>
                            </h5>
                        </div>
                        @else
                        <div class="card-header bg-danger">
                            <h5 style="color:#fff">
                                    <span>Closed</span>
                                <span style="margin: 0px 20px">|</span>
                                <span><i class="fa fa-calendar" style="padding-right: 8px"></i> {{ Carbon\Carbon::parse($forum->created_at)->format('d F Y') }}</span>
                            </h5>
                        </div>
                        @endif
                        <div class="card-body u-line">
                            <a href="/forum/{{$forum->id}}"><h4>{{$forum->judul}}</h4></a>
                            <p class="concat">{{$forum->deskripsi}}</p>
                        </div>
                        <div class="card-footer d-flex justify-content-between footer__copyright__text">
                            <div><i class="fa fa-comments" style="color: #777C81"></i> {{$forum->diskusis->count()}} Obrolan</div>
                            @if ($forum->status_id == 4)
                            <div><a href="/forum/{{$forum->id}}">Bergabung <span class="arrow_right"></span></i></a></div>
                            @else
                            <div><a href="/forum/{{$forum->id}}">Lihat Forum <span class="arrow_right"></span></i></a></div>
                            @endif
                        </div>
                    </div>
                    @endforeach
                @else
                <div class="card">
                    <h4 style="margin: 20px; text-align:center;">- - Tidak ada Forum Terbuka - -</h4>
                </div>
                @endif
            </div>
        </div>
    </div>
</section>
@endsection
