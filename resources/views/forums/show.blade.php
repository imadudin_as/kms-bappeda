@extends('layouts.app')

@section('styles')
    <style>
        .concat div {
            overflow: hidden;
            -ms-text-overflow: ellipsis;
            -o-text-overflow: ellipsis;
            text-overflow: ellipsis;
            white-space: nowrap;
            width: inherit;
            max-width: 100%;
        }

    </style>
@endsection

@section('content')
<section class="con-pad h-striped">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div style="margin-bottom: 10px">
                    <a href="#" onclick="history.go(-1)"><button class="btn btn-secondary"><i class="fa fa-chevron-left" style="padding-right: 15px"></i> Kembali</button></a>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h3>{{$forum->judul}}</h3>
                    </div>
                    <div class="card-body">
                        <p class="concat">{{$forum->deskripsi}}</p>
                    </div>
                    <div class="card-footer d-flex justify-content-between">
                        <div class="align-self-center"><i class="fa fa-calendar"></i> {{$forum->created_at->diffforHumans()}}</div>
                        @if (!Auth::guest())
                        @if (Auth::user()->id == $forum->user_id || Auth::user()->level_id == 1 || Auth::user()->level_id == 2)
                        <div class="d-flex">
                            <div style="margin: 0px 5px">
                                <a href="/forum/{{$forum->id}}/edit"><button style="width: 100px" class="btn btn-warning"><i class="fa fa-pencil"></i> Edit</button></a>
                            </div>
                            <div style="margin: 0px 5px">
                                @if ($forum->status_id == 4)
                                <button type="submit" style="width: 100px" data-toggle="modal" data-target="#tutupModal" class="btn btn-secondary"><i class="fa fa-lock"></i> Tutup</button>

                                <div class="modal fade" id="tutupModal" tabindex="-1" role="dialog" aria-labelledby="tutupModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="tutupModalLabel">Tutup Forum</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                                {!!Form::open(['action' => ['App\Http\Controllers\ForumsController@tutup', $forum->id], 'method' => 'GET'])!!}
                                                    <input type="hidden" name="id" value="{{ $forum->id }}">
                                                    <input type="hidden" name="status_id" value="5">
                                                    <button type="submit" style="width: 100px" class="btn btn-danger">Tutup</button>
                                                {!!Form::close()!!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @else
                                <button type="submit" style="width: 100px" data-toggle="modal" data-target="#bukaModal" class="btn btn-secondary"><i class="fa fa-unlock"></i> Buka</button>

                                <div class="modal fade" id="bukaModal" tabindex="-1" role="dialog" aria-labelledby="bukaModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="bukaModalLabel">Tutup Forum</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                                {!!Form::open(['action' => ['App\Http\Controllers\ForumsController@buka', $forum->id], 'method' => 'GET'])!!}
                                                    <input type="hidden" name="id" value="{{ $forum->id }}">
                                                    <input type="hidden" name="status_id" value="4">
                                                    <button type="submit" style="width: 100px" class="btn btn-success"> Buka</button>
                                                {!!Form::close()!!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                @endif
                            </div>
                            <div style="margin: 0px 5px">
                                <button type="submit" style="width: 100px" data-toggle="modal" data-target="#hapusModal" class="btn btn-danger"><i class="fa fa-trash"></i> Hapus</button>

                                <div class="modal fade" id="hapusModal" tabindex="-1" role="dialog" aria-labelledby="hapusModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="hapusModalLabel">Hapus Dokumen</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                                {!!Form::open(['action' => ['App\Http\Controllers\ForumsController@destroy', $forum->id], 'method' => 'POST'])!!}
                                                    {{Form::hidden('_method', 'DELETE')}}
                                                    <button type="submit" style="width: 100px" class="btn btn-danger">Hapus</button>
                                                {!!Form::close()!!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                                
                        </div>
                        @endif
                        @endif
                    </div>
                </div>
                <br>
                <div class="card">
                    <div class="card-body">
                        <div class="blog__details__comment" style="margin-bottom: 0px; margin-top: 10px">
                            <ul class="list-group list-group-flush" style="margin-bottom: 10px">
                                @forelse($forum->diskusis as $diskusi)
                                <li class="list-group-item" style="padding: 10px 2px 2px 2px">
                                    <div class="blog__details__comment__item" style="margin-bottom: 8px">
                                        <div class="blog__details__comment__item__pic" style="margin-right: 20px">
                                            <img src="/storage/foto/{{$forum->user->foto}}" alt="">
                                        </div>
                                        <div class="blog__details__comment__item__text">
                                            <h5 style="margin-top: 0px">{{$diskusi->user->nama}}</h5>
                                            <p style="margin-bottom: 0px; color: #040404">{{$diskusi->isi}}</p>
                                            <span><i class="fa fa-clock-o" style="padding-right: 10px"></i>{{$diskusi->created_at->diffforHumans()}}</span>
                                        </div>
                                    </div>
                                </li>
                                @empty
                                    @if( !Auth::guest() && $forum->status_id == 4)
                                    <h4 class="align-self-center">Silahkan mulai obrolan</h4>
                                    @endif
                                @endforelse
                            </ul>
                            @if (!Auth::guest() && $forum->status_id == 4)
                                <form action="{{ action('App\Http\Controllers\ForumsController@diskusi_create') }}" method="POST">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="id" value="{{ $forum->id }}">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <textarea type="text" class="form-control" name="isi" placeholder="Ketikkan pesan anda disini..." rows="4"></textarea>
                                        </div>
                                    </div>
                                    <div class="container" style="display: flex; justify-content: flex-end">
                                        <button type="submit" class="btn btn-warning" style="height: 40px; margin-bottom:20px">Kirim</button>
                                    </div>
                                </form>
                            @else
                                <h4 style="text-align: center">Forum telah ditutup</h4>
                            @endif
                        </div>
                    </div>
                </div>
                <br>
            </div>
        </div>
    </div>
</section>
@endsection
