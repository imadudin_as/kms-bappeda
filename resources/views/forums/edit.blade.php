@extends('layouts.app')


@section('content')
<section class="con-pad h-striped">
    <div class="container">
        <div class="card" style="padding: 1rem 0rem">
            <div class="card-body ">
                <a href="#" onclick="history.go(-1)"><button class="btn btn-secondary"><i class="fa fa-chevron-left" style="padding-right: 15px"></i> Kembali</button></a>
            </div>
            <hr>
            <div class="col-12">
                {!! Form::open(['action' => ['App\Http\Controllers\ForumsController@update', $forum->id], 'method' => 'POST']) !!}
                    <div class="form-group row">
                        <label for="judul" class="col-sm-2 col-form-label">Judul Forum</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="judul" id="judul" placeholder="Judul Forum..." value="{{$forum->judul}}" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="deskripsi" class="col-sm-2 col-form-label">Deskripsi</label>
                        <div class="col-sm-10">
                            <textarea type="text" class="form-control" name="deskripsi" id="deskripsi" placeholder="Deskripsi Forum..." rows="4" required>{{$forum->deskripsi}}</textarea>
                        </div>
                    </div>
                    <hr>
                    {{Form::hidden('_method', 'PUT')}}
                    <div class="container" style="display: flex; justify-content: flex-end">
                        {{Form::submit('Simpan', ['class' => 'btn btn-primary', 'style' => 'height: 40px; margin-bottom:20px;'])}}
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</section>
@endsection
