@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
@endsection

@section('content')
<section class="con-pad h-striped">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3>Dashboard</h3>    
                    </div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col-6">
                                @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                                @endif
        
                                {{ __('You are logged in!') }}
                            </div>
                            <div class="col-6" style="display: flex; justify-content: flex-end">
                                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><button class="btn btn-warning" style="color: white">Logout</button></a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <br>
        @if (!Auth::guest())
        <div style="display: flex; justify-content: flex-end; margin-bottom: 1rem">
            <a href="/docs/create"><button class="btn btn-info"><i class="fa fa-plus" style="padding-right"></i> Upload Dokumen</button></a>
        </div>
        @endif
        <br>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3>Dokumen Anda</h3>    
                    </div>

                    <div class="card-body">
                        <table class="table" id="datatable">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Nama</th>
                                    <th>Tahun</th>
                                    <th>Diupload Tanggal</th>
                                    <th>Tipe File</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($docs as $index => $doc)    
                                <tr>
                                    <td>{{$index +1}}</td>
                                    <td>{{$doc->nama}}</td>
                                    <td>{{$doc->tahun}}</td>
                                    <td>{{ Carbon\Carbon::parse($doc->created_at)->format('d F Y')}}</td>
                                    <td>{{$doc->jenis_file}}</td>
                                    <td>
                                        <a type="button" href="/docs/{{$doc->id}}"><button class="btn btn-success"><i style="color: #fff;" class="fa fa-eye"></i></button></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('javascripts')
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready( function () {
            $('#datatable').DataTable({
                "language": {
                    "decimal":        "",
                    "emptyTable":     "Anda Belum Mengupload Dokumen",
                    "info":           "Menampilkan _START_ hingga _END_ dari _TOTAL_ data",
                    "infoEmpty":      "Menampilkan 0 hingga 0 dari 0 data",
                    "infoFiltered":   "(dari total _MAX_ data)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Menampilkan _MENU_ data",
                    "loadingRecords": "Loading...",
                    "processing":     "Processing...",
                    "search":         "Cari:",
                    "zeroRecords":    "Dokumen Tidak Ditemukan",
                    "paginate": {
                        "next":       ">",
                        "previous":   "<"
                    },
                    "aria": {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    }
                }
            });
        });
    </script>
@endsection