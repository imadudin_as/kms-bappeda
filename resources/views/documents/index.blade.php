@extends('layouts.app')


@section('content')
<section class="con-pad h-striped">
    <div class="container">
        <div class="footer__newslatter" style="width: 100%;">
            <form action="{{ action('App\Http\Controllers\DocumentsController@search') }}" method="GET">
                <input type="text" placeholder="Cari Dokumen..." name="cari">
                <button type="submit"><span><i class="fa fa-search" style="margin-right: 5px"></i> Cari </span></button>
            </form>
        </div>
        @include('inc.messages')
        <div class="row">
            <div class="col-6 col-md-3">
                <br>
                <div class="card">
                    <div class="card-header">
                        <h4>Tahun</h4>
                    </div>
                    <div class="card-body blog__sidebar__categories">
                        <ul>
                            <li><a href="/docs/search?cari=2015">2015</a></li>
                            <li><a href="/docs/search?cari=2016">2016</a></li>
                            <li><a href="/docs/search?cari=2017">2017</a></li>
                            <li><a href="/docs/search?cari=2018">2018</a></li>
                            <li><a href="/docs/search?cari=2019">2019</a></li>
                            <li><a href="/docs/search?cari=2020">2020</a></li>
                        </ul>
                    </div>
                </div>
                <br>
                <div class="card">
                    <div class="card-header">
                        <h4>Tipe File</h4>
                    </div>
                    <div class="card-body blog__sidebar__categories">
                        <ul>
                            <li><a href="/docs/search?cari=pdf">PDF</a></li>
                            <li><a href="/docs/search?cari=doc">DOC</a></li>
                            <li><a href="/docs/search?cari=ppt">PPT</a></li>
                            <li><a href="/docs/search?cari=excel">EXCEL</a></li>
                            <li><a href="/docs/search?cari=zip">ZIP</a></li>
                            <li><a href="/docs/search?cari=rar">RAR</a></li>
                            <li><a href="/docs/search?cari=txt">TXT</a></li>
                            <li><a href="/docs/search?cari=jpg">JPG</a></li>
                            <li><a href="/docs/search?cari=png">PNG</a></li>
                            <li><a href="/docs/search?cari=mp4">MP4</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-9">
                @if (!Auth::guest())
                <div style="display: flex; justify-content: flex-end; margin-bottom: 1rem">
                    <a href="/docs/create"><button class="btn btn-info"><i class="fa fa-plus" style="padding-right"></i> Upload Dokumen</button></a>
                </div>
                @endif
                <div class="card">
                    <div class="card-header">
                        <h3>Daftar Dokumen</h3>
                    </div>
                    <div class="list-group">
                        @if (count($docs) > 0)
                            @foreach ($docs as $doc)
                            <a href="/docs/{{ $doc->id }}" class="list-group-item list-group-item-action">
                                <div class="row">
                                    <div class="col-2" style="display: flex; justify-content: center">
                                        <img src="{{ asset('img') }}/png/{{ $doc->jenis_file }}.png" alt="" style="width: 60%">
                                    </div>
                                    <div class="col-10 align-self-center">
                                        {{$doc->nama}}
                                        <br>
                                        <small>Oleh : {{$doc->user->nama}}</small>
                                    </div>

                                </div>
                            </a>
                            @endforeach
                        @else
                            <div class="col-12 d-flex justify-content-center">
                                <h4 style="margin: 20px">- - Tidak ada dokumen - -</h4>
                            </div>
                        @endif
                        
                        
                        {{$docs->links()}}
                    </div>
                  </div>
            </div>
        </div>
    </div>
</section>
@endsection
