@extends('layouts.app')


@section('content')
<section class="con-pad h-striped">
    <div class="container">
        <div class="card" style="padding: 1rem 0rem">
            <div class="card-body row">
                <div class="col-3">
                    <a href="#" onclick="history.go(-1)"><button class="btn btn-secondary"><i class="fa fa-chevron-left" style="padding-right: 15px"></i> Kembali</button></a>
                </div>
                <div class="col-9 row justify-content-end">
                    @if (!Auth::guest())
                        @if (Auth::user()->id == $doc->user_id || Auth::user()->level_id == 1 || Auth::user()->level_id == 2)
                            <a href="/docs/{{$doc->id}}/edit"><button class="btn btn-warning" style="margin: 0 0.5rem"><i class="fa fa-pencil" style="padding-right: 15px"></i> Edit</button></a>
                            <button type="submit" class="btn btn-danger" data-toggle="modal" data-target="#hapusModal" style="margin: 0 0.5rem"><i class="fa fa-trash" style="padding-right: 15px"></i> Hapus</button>

                            <div class="modal fade" id="hapusModal" tabindex="-1" role="dialog" aria-labelledby="hapusModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="hapusModalLabel">Hapus Dokumen</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                            {!!Form::open(['action' => ['App\Http\Controllers\DocumentsController@destroy', $doc->id], 'method' => 'POST'])!!}
                                                {{Form::hidden('_method', 'DELETE')}}
                                                <button type="submit" class="btn btn-danger" style="margin: 0 0.5rem">Hapus</button>
                                            {!!Form::close()!!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        @endif
                    @endif
                    
                </div>
            </div>
            <hr>
            <div class="col-12 row">
                <div class="col-md-3 align-self-center">
                    {{-- <embed width="100%" height="500px" name="plugin" src="/storage/file/{{$doc->file}}" type="application/pdf" style="overflow: hidden"> --}}
                    {{-- <iframe src="/storage/file/{{$doc->file}}" frameborder="0" height="500px" width="100%"></iframe> --}}
                    <img src="{{ asset('img') }}/png/{{$doc->jenis_file}}.png" alt="">
                </div>
                <div class="col-md-9" style="border-left: 0.5px solid">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-sm-3">
                                    Nama Dokumen
                                </div>
                                <div class="col-auto">
                                    :
                                </div>
                                <div class="col-sm-8">
                                    {{ $doc->nama }}
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-sm-3">
                                    Author
                                </div>
                                <div class="col-auto">
                                    :
                                </div>
                                <div class="col-sm-8">
                                    {{ $doc->user->nama }}
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-sm-3">
                                    Diupload Tanggal
                                </div>
                                <div class="col-auto">
                                    :
                                </div>
                                <div class="col-sm-8">
                                    {{ Carbon\Carbon::parse($doc->created_at)->format('d F Y') }}
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-sm-3">
                                    Halaman
                                </div>
                                <div class="col-auto">
                                    :
                                </div>
                                <div class="col-sm-8">
                                    {{ $doc->halaman }} Halaman
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-sm-3">
                                    Tahun
                                </div>
                                <div class="col-auto">
                                    :
                                </div>
                                <div class="col-sm-8">
                                    {{ $doc->tahun }}
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-sm-3">
                                    Deskripsi
                                </div>
                                <div class="col-auto">
                                    :
                                </div>
                                <div class="col-sm-8">
                                    {{ $doc->deskripsi }}
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-sm-3">
                                    Validasi
                                </div>
                                <div class="col-auto">
                                    :
                                </div>
                                <div class="col-sm-8">
                                    {{ $doc->status->nama }}
                                </div>
                            </div>
                        </li>
                    </ul>
                    <br>
                    <div class="row justify-content-end">
                        <a href="/storage/file/{{$doc->file}}" download=""><button class="btn btn-primary"><i class="fa fa-download" style="padding-right: 15px"></i> Download</button></a>
                    </div>
                </div>
            </div>
            <hr>
            <div class="card-body">
                @if (!Auth::guest())    
                <div class="blog__details__comment__form">
                    <h3>Tinggalkan Komentar</h3>
                    <form action="{{ action('App\Http\Controllers\DocumentsController@comment_create') }}" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{ $doc->id }}">
                        <div style="display: flex">
                            <div class="col-10" style="padding: 0px">
                                <input placeholder="Messages" name="isi">
                            </div>
                            <div class="col-2" style="padding: 0px">
                                <button type="submit" style="height: 50px; width: 100%">Kirim <i class="fa fa-chevron-circle-right"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
                @endif
               
                <div class="blog__details__comment">
                    <h3>{{$doc->komentars->count()}} Komentar</h3>
                    @forelse($doc->komentars as $komentar)
                        <div class="blog__details__comment__item">
                            <div class="blog__details__comment__item__pic">
                                <img src="/storage/foto/{{$komentar->user->foto}}" alt="">
                            </div>
                            <div class="blog__details__comment__item__text">
                                <div class="row">
                                    <div class="col-10">
                                        <span>{{$komentar->created_at->diffforHumans()}}</span>
                                        <h5>{{ $komentar->user->nama }}</h5>
                                        <p style="color: black">{{ $komentar->isi }}</p>
                                    </div>
                                    @if (!Auth::guest())
                                    <div class="col-2">
                                        <div class="row">
                                            {{-- Modal Edit --}}
                                            @if (Auth::user()->id == $komentar->user_id)
                                            <button class="btn btn-warning" data-toggle="modal" data-target="#comment-modal-{{ $komentar->id }}" data-modal="{{ $komentar->id }}" style="margin: 2px"> Edit</button>
                                            @endif
                                            <div class="modal fade" id="comment-modal-{{ $komentar->id }}" tabindex="-1" role="dialog" aria-labelledby="editKomenModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <form action="{{ action('App\Http\Controllers\DocumentsController@comment_update',$komentar->id) }}" method="POST">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" id="editKomenModalLabel">Edit Dokumen</h4>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                {{ csrf_field() }}
                                                                <div class="form-group">	
                                                                    <input type="text" class="form-control" name="isi" value="{{ $komentar->isi }}">
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                                                <button class="btn btn-success" style="margin: 0 0.5rem">Edit</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            {{-- Modal Hapus --}}
                                            @if (Auth::user()->id == $komentar->user_id || Auth::user()->level_id == 1 || Auth::user()->level_id == 2)
                                            <button class="btn btn-danger" data-toggle="modal" data-target="#hapus-modal-{{ $komentar->id }}" data-modal="{{ $komentar->id }}" style="margin: 2px"> Hapus</button>
                                            @endif
                                            <div class="modal fade" id="hapus-modal-{{ $komentar->id }}" tabindex="-1" role="dialog" aria-labelledby="hapusKomenModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" id="hapusKomenModalLabel">Hapus Komentar</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                                            <a href="{{ action('App\Http\Controllers\DocumentsController@comment_delete',$komentar->id) }}">
                                                                <button class="btn btn-danger">Hapus</button>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @empty
                        <h4>Jadilah yang pertama untuk berkomentar!</h4>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
