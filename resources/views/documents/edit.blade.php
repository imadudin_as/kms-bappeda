@extends('layouts.app')


@section('content')
<section class="con-pad h-striped">
    <div class="container">
        <div class="card" style="padding: 1rem 0rem">
            <div class="card-body ">
                <a href="#" onclick="history.go(-1)"><button class="btn btn-secondary"><i class="fa fa-chevron-left" style="padding-right: 15px"></i> Kembali</button></a>
            </div>
            <hr>
            <div class="col-12">
                {!! Form::open(['action' => ['App\Http\Controllers\DocumentsController@update', $doc->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                    <div class="form-group row">
                        <label for="nama" class="col-sm-2 col-form-label">Judul</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama Dokumen..." value="{{$doc->nama}}" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="halaman" class="col-sm-2 col-form-label">Halaman</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="halaman" id="halaman" placeholder="Jumlah Halaman..." value="{{$doc->halaman}}" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="deskripsi" class="col-sm-2 col-form-label">Deskripsi</label>
                        <div class="col-sm-10">
                            <textarea type="text" class="form-control" id="deskripsi" name="deskripsi" placeholder="Deskripsi File..." rows="4" required>{{$doc->deskripsi}}</textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="tahun" class="col-sm-2 col-form-label">Tahun</label>
                        <div class="col-sm-10">
                            {{Form::select('tahun', [
                                '2015' => '2015',
                                '2016' => '2016', 
                                '2017' => '2017', 
                                '2018' => '2018',
                                '2019' => '2019',
                                '2020' => '2020',
                                ], $doc->tahun, ['class' => 'form-control', 'required'])}}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="jenis_file" class="col-sm-2 col-form-label">Tipe File</label>
                        <div class="col-sm-10">
                            {{Form::select('jenis_file', [
                                'PDF' => 'PDF',
                                'DOCS' => 'DOCS', 
                                'EXCEL' => 'EXCEL', 
                                'PPT' => 'PPT',
                                'ZIP' => 'ZIP',
                                'RAR' => 'RAR',
                                'TXT' => 'TXT',
                                'MP4' => 'MP4',
                                ], $doc->jenis_file, ['class' => 'form-control', 'required'])}}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="file" class="col-sm-2 col-form-label">Upload File</label>
                        <div class="col-sm-10">
                            <input type="file" class="form-control-file" id="file" name="file">
                        </div>
                    </div>
                    <hr>
                    {{Form::hidden('_method', 'PUT')}}
                    <div class="container" style="display: flex; justify-content: flex-end">
                        {{Form::submit('Simpan', ['class' => 'btn btn-primary', 'style' => 'height: 40px; margin-bottom:20px;'])}}
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</section>
@endsection
