@extends('layouts.app')


@section('content')
<section class="con-pad h-striped">
    <div class="container">
        @include('inc.messages')
        <div class="card" style="padding: 1rem 0rem">
            <div class="card-body ">
                <a href="#" onclick="history.go(-1)"><button class="btn btn-secondary"><i class="fa fa-chevron-left" style="padding-right: 15px"></i> Kembali</button></a>
            </div>
            <hr>
            <div class="col-12">
                {!! Form::open(['action' => 'App\Http\Controllers\DocumentsController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                    <div class="form-group row">
                        <label for="nama" class="col-sm-2 col-form-label">Judul</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama Dokumen..." required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="halaman" class="col-sm-2 col-form-label">Halaman</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="halaman" id="halaman" placeholder="Jumlah Halaman..." required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="deskripsi" class="col-sm-2 col-form-label">Deskripsi</label>
                        <div class="col-sm-10">
                            <textarea type="text" class="form-control" name="deskripsi" id="deskripsi" placeholder="Deskripsi File..." rows="4" required></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="tahun" class="col-sm-2 col-form-label">Tahun</label>
                        <div class="col-sm-10">
                            <select class="form-control" id="tahun" name="tahun" required>
                                <option value="2015">2015</option>
                                <option value="2016">2016</option>
                                <option value="2017">2017</option>
                                <option value="2018">2018</option>
                                <option value="2019">2019</option>
                                <option value="2020">2020</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="jenis_file" class="col-sm-2 col-form-label">Tipe File</label>
                        <div class="col-sm-10">
                            <select class="form-control" id="jenis_file" name="jenis_file" required>
                                <option value="PDF">PDF</option>
                                <option value="DOCS">DOCS</option>
                                <option value="EXCEL">EXCEL</option>
                                <option value="PPT">PPT</option>
                                <option value="ZIP">ZIP</option>
                                <option value="RAR">RAR</option>
                                <option value="TXT">TXT</option>
                                <option value="MP4">MP4</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="file" class="col-sm-2 col-form-label">Upload File</label>
                        <div class="col-sm-10">
                            <input type="file" class="form-control-file" id="file" name="file" required>
                        </div>
                    </div>
                    <hr>
                    <div class="container" style="display: flex; justify-content: flex-end">
                        {{Form::submit('Simpan', ['class' => 'btn btn-primary', 'style' => 'height: 40px; margin-bottom:20px;'])}}
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</section>
@endsection
