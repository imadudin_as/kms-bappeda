@extends('layouts.app')


@section('content')
<section class="con-pad h-striped">
    <div class="container">
        <div class="footer__newslatter" style="width: 100%;">
            <form action="{{ action('App\Http\Controllers\DocumentsController@search') }}" method="GET">
                <input type="text" placeholder="Cari Dokumen..." name="cari">
                <button type="submit"><span><i class="fa fa-search" style="margin-right: 5px"></i> Cari </span></button>
            </form>
            <div class="card" style="margin-top: -15px;">
                <div class="card-body">
                    <p style="margin-bottom: 0; color: black;">Menampilkan Hasil Pencarian : {{$search}}</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="list-group">
                        @if(count($docs) > 0)
                            @foreach ($docs as $doc)    
                            <a href="/docs/{{$doc->id}}" class="list-group-item list-group-item-action">
                                <div class="row">
                                    <div class="col-1" style="display: flex; justify-content: center">
                                        <img src="{{ asset('img') }}/png/{{$doc->jenis_file}}.png" alt="" style="width: 100%">
                                    </div>
                                    <div class="col-10 align-self-center">
                                        {{$doc->nama}}
                                        <br>
                                        <small>Oleh : {{$doc->user->nama}}</small>
                                    </div>
                                </div>
                            </a>
                            @endforeach
                        @else
                            <div class="col-12 d-flex justify-content-center">
                                <h4 style="margin: 20px">- - File tidak ditemukan - -</h4>
                            </div>
                        @endif
                    </div>
                  </div>
            </div>
        </div>
    </div>
</section>
@endsection
