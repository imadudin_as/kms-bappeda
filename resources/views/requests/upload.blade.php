@extends('layouts.app')


@section('content')
<section class="con-pad h-striped">
    <div class="container">
        <div class="card" style="padding: 1rem 0rem">
            <div class="card-body ">
                <a href="#" onclick="history.go(-1)"><button class="btn btn-secondary"><i class="fa fa-chevron-left" style="padding-right: 15px"></i> Kembali</button></a>
            </div>
            <hr>
            <div class="col-12">
                {!! Form::open(['action' => 'App\Http\Controllers\DocumentsController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                    <div class="form-group row">
                        <label for="judul" class="col-sm-2 col-form-label">Judul</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="judul" placeholder="Nama Dokumen...">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="halaman" class="col-sm-2 col-form-label">Halaman</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="halaman" placeholder="Jumlah Halaman...">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="article-ckeditor" class="col-sm-2 col-form-label">Deskripsi</label>
                        <div class="col-sm-10">
                            <textarea type="text" class="form-control" id="article-ckeditor" placeholder="Deskripsi File..." rows="4"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="tipe" class="col-sm-2 col-form-label">Tipe File</label>
                        <div class="col-sm-10">
                            <select class="form-control" id="exampleFormControlSelect1">
                                <option>PDF</option>
                                <option>Word</option>
                                <option>Excel</option>
                                <option>PPT</option>
                                <option>ZIP</option>
                                <option>TXT</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="file_dokumen" class="col-sm-2 col-form-label">Upload File</label>
                        <div class="col-sm-10">
                            <input type="file" class="form-control-file" id="file_dokumen">
                        </div>
                    </div>
                    <hr>
                    <div class="container" style="display: flex; justify-content: flex-end">
                        {{Form::submit('Simpan', ['class' => 'btn btn-primary', 'style' => 'height: 40px; margin-bottom:20px;'])}}
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</section>
@endsection
