@extends('layouts.app')


@section('content')
<section class="con-pad h-striped">
    <div class="container">
        @if (!Auth::guest()) 
        <div style="display: flex; justify-content: flex-end; margin-bottom: 1rem">
            <a href="/request/create"><button class="btn btn-info"><i class="fa fa-plus" style="padding-right"></i> Tambah Request</button></a>
        </div>
        @endif
        @include('inc.messages')
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3>Daftar Request</h3>
                    </div>
                    <div class="list-group">
                        @if(count($reqs) > 0)
                            @foreach ($reqs as $req)
                                <a href="/request/{{$req->id}}" class="list-group-item list-group-item-action">
                                    <div class="row">
                                        <div class="col-1" style="display: flex; justify-content: center">
                                            <img src="{{ asset('img') }}/png/{{$req->jenis_file}}.png" alt="" style="width: 100%">
                                        </div>
                                        <div class="col-10 align-self-center">
                                            {{$req->nama}}
                                            <br>
                                            <small>Oleh : {{$req->user->nama}}</small>
                                        </div>
                                    </div>
                                </a>
                            @endforeach
                        @else
                            <div class="col-12 d-flex justify-content-center">
                                <h4 style="margin: 20px">- - Tidak ada Request - -</h4>
                            </div>
                        @endif
                    </div>
                  </div>
            </div>
        </div>
    </div>
</section>
@endsection
