@extends('layouts.app')


@section('content')
<section class="con-pad h-striped">
    <div class="container">
        <div class="card" style="padding: 1rem 0rem">
            <div class="card-body ">
                <a href="#" onclick="history.go(-1)"><button class="btn btn-secondary"><i class="fa fa-chevron-left" style="padding-right: 15px"></i> Kembali</button></a>
            </div>
            <hr>
            <div class="col-12">
                {!! Form::open(['action' => ['App\Http\Controllers\RequestsController@update', $req->id], 'method' => 'POST']) !!}
                    <div class="form-group row">
                        <label for="nama" class="col-sm-2 col-form-label">Nama Request</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama Request..." value="{{$req->nama}}" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="deskripsi" class="col-sm-2 col-form-label">Deskripsi</label>
                        <div class="col-sm-10">
                            <textarea type="text" class="form-control" name="deskripsi" id="deskripsi" placeholder="Deskripsi File..." rows="4" required>{{$req->deskripsi}}</textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="jenis_file" class="col-sm-2 col-form-label">Tipe File</label>
                        <div class="col-sm-10">
                            {{Form::select('jenis_file', [
                                'PDF' => 'PDF',
                                'DOCS' => 'DOCS', 
                                'EXCEL' => 'EXCEL', 
                                'PPT' => 'PPT',
                                'ZIP' => 'ZIP',
                                'RAR' => 'RAR',
                                'TXT' => 'TXT',
                                'MP4' => 'MP4',
                                ], $req->jenis_file, ['class' => 'form-control', 'required'])}}
                        </div>
                    </div>
                    <hr>
                    {{Form::hidden('_method', 'PUT')}}
                    <div class="container" style="display: flex; justify-content: flex-end">
                        {{Form::submit('Simpan', ['class' => 'btn btn-primary', 'style' => 'height: 40px; margin-bottom:20px;'])}}
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</section>
@endsection
