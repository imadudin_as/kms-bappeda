@extends('layouts.app')


@section('content')
<section class="con-pad h-striped">
    <div class="container">
        <div class="card" style="padding: 1rem 0rem">
            <div class="card-body ">
                <a href="#" onclick="history.go(-1)"><button class="btn btn-secondary"><i class="fa fa-chevron-left" style="padding-right: 15px"></i> Kembali</button></a>
            </div>
            <hr>
            <div class="col-12">
                {!! Form::open(['action' => 'App\Http\Controllers\RequestsController@store', 'method' => 'POST']) !!}
                    <div class="form-group row">
                        <label for="nama" class="col-sm-2 col-form-label">Nama Request</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama Request..." required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="deskripsi" class="col-sm-2 col-form-label">Deskripsi</label>
                        <div class="col-sm-10">
                            <textarea type="text" class="form-control" name="deskripsi" id="deskripsi" placeholder="Deskripsi File..." rows="4" required></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="jenis_file" class="col-sm-2 col-form-label">Tipe File</label>
                        <div class="col-sm-10">
                            <select class="form-control" id="jenis_file" name="jenis_file" required>
                                <option value="PDF">PDF</option>
                                <option value="DOCS">DOCS</option>
                                <option value="EXCEL">EXCEL</option>
                                <option value="PPT">PPT</option>
                                <option value="ZIP">ZIP</option>
                                <option value="RAR">RAR</option>
                                <option value="TXT">TXT</option>
                                <option value="MP4">MP4</option>
                            </select>
                        </div>
                    </div>
                    <hr>
                    <div class="container" style="display: flex; justify-content: flex-end">
                        {{Form::submit('Simpan', ['class' => 'btn btn-primary', 'style' => 'height: 40px; margin-bottom:20px;'])}}
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</section>
@endsection
