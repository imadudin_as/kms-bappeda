@extends('layouts.app')


@section('content')
<section class="con-pad h-striped">
    <div class="container">
        <div class="card" style="padding: 1rem 0rem">
            <div class="card-body row">
                <div class="col-3">
                    <a href="/request""><button class="btn btn-secondary"><i class="fa fa-chevron-left" style="padding-right: 15px"></i> Kembali</button></a>
                </div>
                <div class="col-9 row justify-content-end">
                    @if (!Auth::guest())
                        @if (Auth::user()->id == $req->user_id || Auth::user()->level_id == 1 || Auth::user()->level_id == 2)
                            <a href="/request/{{$req->id}}/edit"><button class="btn btn-warning" style="margin: 0 0.5rem"><i class="fa fa-pencil" style="padding-right: 15px"></i> Edit</button></a>
                        @endif
                        @if (!Auth::guest())
                            {!!Form::open(['action' => ['App\Http\Controllers\RequestsController@selesai', $req->id], 'method' => 'GET'])!!}
                            <input type="hidden" name="id" value="{{ $req->id }}">
                            <input type="hidden" name="status_id" value="7">
                            <button type="submit" class="btn btn-success" style="margin: 0 0.5rem"><i class="fa fa-check" style="padding-right: 15px"></i> Selesai</button>
                            {!!Form::close()!!}
                        @endif
                    @endif
                </div>
            </div>
            <hr>
            <div class="col-12 row">
                <div class="col-md-3 align-self-center">
                    <img src="{{ asset('img') }}/png/{{$req->jenis_file}}.png" alt="">
                </div>
                <div class="col-md-9" style="border-left: 0.5px solid">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-sm-3">
                                    Nama Request
                                </div>
                                <div class="col-auto">
                                    :
                                </div>
                                <div class="col-sm-8">
                                    {{$req->nama}}
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-sm-3">
                                    Nama Perequest
                                </div>
                                <div class="col-auto">
                                    :
                                </div>
                                <div class="col-sm-8">
                                    {{$req->user->nama}}
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-sm-3">
                                    Tanggal
                                </div>
                                <div class="col-auto">
                                    :
                                </div>
                                <div class="col-sm-8">
                                    {{ Carbon\Carbon::parse($req->created_at)->format('d F Y') }}
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-sm-3">
                                    Deskripsi
                                </div>
                                <div class="col-auto">
                                    :
                                </div>
                                <div class="col-sm-8">
                                    {{$req->deskripsi}}
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-sm-3">
                                    Tipe File
                                </div>
                                <div class="col-auto">
                                    :
                                </div>
                                <div class="col-sm-8">
                                    {{$req->jenis_file}}
                                </div>
                            </div>
                        </li>
                    </ul>
                    <br>
                    <div class="row justify-content-end">
                        <a href="/docs/create"><button class="btn btn-primary"><i class="fa fa-upload" style="padding-right: 15px"></i> Upload File</button></a>
                    </div>
                </div>
            </div>
            <hr>
            <div class="card-body">
                @if (!Auth::guest())    
                <div class="blog__details__comment__form">
                    <h3>Tinggalkan Komentar</h3>
                    <form action="{{ action('App\Http\Controllers\RequestsController@req_comment_create') }}" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{ $req->id }}">
                        <div style="display: flex">
                            <div class="col-10" style="padding: 0px">
                                <input placeholder="Messages" name="isi">
                            </div>
                            <div class="col-2" style="padding: 0px">
                                <button type="submit" style="height: 50px; width: 100%">Kirim <i class="fa fa-chevron-circle-right"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
                @endif

                <div class="blog__details__comment">
                    <h3>{{$req->req_komentars->count()}} Komentar</h3>
                    @forelse ($req->req_komentars as $komentar)
                        <div class="blog__details__comment__item">
                            <div class="blog__details__comment__item__pic">
                                <img src="/storage/foto/{{$komentar->user->foto}}" alt="">
                            </div>
                            <div class="blog__details__comment__item__text">
                                <div class="row">
                                    <div class="col 10">
                                        <span>{{$komentar->created_at->diffforHumans()}}</span>
                                        <h5>{{ $komentar->user->nama }}</h5>
                                        <p style="color: black">{{ $komentar->isi }}</p>
                                    </div>
                                    @if (!Auth::guest())
                                    <div class="col-2">
                                        <div class="row">
                                            {{-- Modal Edit --}}
                                            @if (Auth::user()->id == $komentar->user_id)
                                            <button class="btn btn-warning" data-toggle="modal" data-target="#comment-modal-{{ $komentar->id }}" data-modal="{{ $komentar->id }}" style="margin: 2px"> Edit</button>
                                            @endif
                                            <div class="modal fade" id="comment-modal-{{ $komentar->id }}" tabindex="-1" role="dialog" aria-labelledby="editKomenModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <form action="{{ action('App\Http\Controllers\RequestsController@req_comment_update',$komentar->id) }}" method="POST">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" id="editKomenModalLabel">Edit Dokumen</h4>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                {{ csrf_field() }}
                                                                <div class="form-group">	
                                                                    <input type="text" class="form-control" name="isi" value="{{ $komentar->isi }}">
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                                                <button class="btn btn-success" style="margin: 0 0.5rem">Edit</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            {{-- Modal Hapus --}}
                                            @if (Auth::user()->id == $komentar->user_id || Auth::user()->level_id == 1 || Auth::user()->level_id == 2)
                                            <button class="btn btn-danger" data-toggle="modal" data-target="#hapus-modal-{{ $komentar->id }}" data-modal="{{ $komentar->id }}" style="margin: 2px"> Hapus</button>
                                            @endif
                                            <div class="modal fade" id="hapus-modal-{{ $komentar->id }}" tabindex="-1" role="dialog" aria-labelledby="hapusKomenModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" id="hapusKomenModalLabel">Hapus Komentar</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                                            <a href="{{ action('App\Http\Controllers\RequestsController@req_comment_delete',$komentar->id) }}">
                                                                <button class="btn btn-danger">Hapus</button>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @empty
                        <h4>Jadilah yang pertama untuk berkomentar!</h4>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
