@extends('layouts.app')


@section('content')
<section class="con-pad h-striped">
    <div class="container">
        <div class="card" style="padding: 1rem 0rem">
            <div class="card-body row">
                <div class="col-3">
                    <a href="#" onclick="history.go(-1)"><button class="btn btn-secondary"><i class="fa fa-chevron-left" style="padding-right: 15px"></i> Kembali</button></a>
                </div>
                <div class="col-9 row justify-content-end">
                    <a href="/users/{{$user->id}}/edit"><button class="btn btn-warning" style="margin: 0 0.5rem"><i class="fa fa-pencil" style="padding-right: 15px"></i> Edit</button></a>
                </div>
            </div>
            <hr>
            <div class="col-12 row">
                <div class="col-md-3 align-self-center">
                    <img src="/storage/foto/{{$user->foto}}" alt="Foto tidak muncul">
                </div>
                <div class="col-md-9" style="border-left: 0.5px solid">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-sm-3">
                                    Nama
                                </div>
                                <div class="col-auto">
                                    :
                                </div>
                                <div class="col-sm-8">
                                    {{$user->nama}}
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-sm-3">
                                    NIP
                                </div>
                                <div class="col-auto">
                                    :
                                </div>
                                <div class="col-sm-8">
                                    {{$user->NIP}}
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-sm-3">
                                    E-Mail
                                </div>
                                <div class="col-auto">
                                    :
                                </div>
                                <div class="col-sm-8">
                                    {{$user->email}}
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-sm-3">
                                    Jabatan
                                </div>
                                <div class="col-auto">
                                    :
                                </div>
                                <div class="col-sm-8">
                                    {{$user->jabatan->nama}}
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-sm-3">
                                    Bidang
                                </div>
                                <div class="col-auto">
                                    :
                                </div>
                                <div class="col-sm-8">
                                    {{$user->bidang->nama}}
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-sm-3">
                                    Jenis Kelamin
                                </div>
                                <div class="col-auto">
                                    :
                                </div>
                                <div class="col-sm-8">
                                    {{$user->jenis_kelamin}}
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-sm-3">
                                    TTL
                                </div>
                                <div class="col-auto">
                                    :
                                </div>
                                <div class="col-sm-8">
                                    {{$user->tempat_lahir}}/{{Carbon\Carbon::parse($user->tgl_lahir)->format('d F Y')}}
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-sm-3">
                                    No. HP
                                </div>
                                <div class="col-auto">
                                    :
                                </div>
                                <div class="col-sm-8">
                                    {{$user->no_hp}}
                                </div>
                            </div>
                        </li>
                    </ul>
                    <br>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
