@extends('layouts.app')


@section('content')
<section class="con-pad h-striped">
    @include('inc.messages')
    <div class="container">
        <div class="card" style="padding: 1rem 0rem">
            <div class="card-body ">
                <a href="#" onclick="history.go(-1)"><button class="btn btn-secondary"><i class="fa fa-chevron-left" style="padding-right: 15px"></i> Kembali</button></a>
            </div>
            <hr>
            <div class="col-12">
                {!! Form::open(['action' => ['App\Http\Controllers\UsersController@update', $user->id], 'method' => 'POST']) !!}
                    <div class="form-group row">
                        <label for="nama" class="col-sm-2 col-form-label">Nama</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama Request..." value="{{$user->nama}}" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="NIP" class="col-sm-2 col-form-label">NIP</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" name="NIP" id="NIP" placeholder="NIP" value="{{$user->NIP}}" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="jenis_kelamin" class="col-sm-2 col-form-label">Jenis Kelamin</label>
                        <div class="col-sm-10">
                            <select name="jenis_kelamin" id="jenis_kelamin" class="form-control">
                                <option value="Laki-Laki" @if ($user->jenis_kelamin == 'Laki-Laki')selected @endif>Laki-Laki</option>
                                <option value="Perempuan" @if ($user->jenis_kelamin == 'Perempuan')selected @endif>Perempuan</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="jabatan_id" class="col-sm-2 col-form-label">Jabatan</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="jabatan_id" id="jabatan_id" required>
                                @foreach ($jabatans as $jabatan)
                                <option 
                                    value="{{$jabatan->id}}"
                                    @if ($jabatan->id == $user->jabatan_id)
                                    selected
                                    @endif
                                    >{{$jabatan->nama}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="bidang_id" class="col-sm-2 col-form-label">Bidang</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="bidang_id" id="bidang_id" required>
                                @foreach ($bidangs as $bidang)
                                <option 
                                    value="{{$bidang->id}}"
                                    @if ($bidang->id == $user->bidang_id)
                                    selected
                                    @endif
                                    >{{$bidang->nama}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="level_id" class="col-sm-2 col-form-label">Level</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="level_id" id="level_id" required>
                                @foreach ($levels as $level)
                                <option 
                                    value="{{$level->id}}"
                                    @if ($level->id == $user->level_id)
                                    selected
                                    @endif
                                    >{{$level->nama}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="email" class="col-sm-2 col-form-label">E-Mail</label>
                        <div class="col-sm-10">
                            <input type="email" class="form-control" name="email" id="email" placeholder="E-Mail..." value="{{$user->email}}" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="password" class="col-sm-2 col-form-label">Password</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                        </div>
                    </div>
                    <hr>
                    {{Form::hidden('_method', 'PUT')}}
                    <div class="container" style="display: flex; justify-content: flex-end">
                        {{Form::submit('Simpan', ['class' => 'btn btn-primary', 'style' => 'height: 40px; margin-bottom:20px;'])}}
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</section>
@endsection
