@extends('layouts.app')


@section('content')
<section class="con-pad h-striped">
    <div class="container">
        <div class="card" style="padding: 1rem 0rem">
            <div class="card-body ">
                <a href="#" onclick="history.go(-1)"><button class="btn btn-secondary"><i class="fa fa-chevron-left" style="padding-right: 15px"></i> Kembali</button></a>
            </div>
            <hr>
            <div class="col-12">
                {!! Form::open(['action' => 'App\Http\Controllers\UsersController@store', 'method' => 'POST']) !!}
                    <div class="form-group row">
                        <label for="nama" class="col-sm-2 col-form-label">Nama</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="NIP" class="col-sm-2 col-form-label">NIP</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" name="NIP" id="NIP" placeholder="NIP">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="jenis_kelamin" class="col-sm-2 col-form-label">Jenis Kelamin</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="jenis_kelamin" id="jenis_kelamin">
                                <option>Laki-Laki</option>
                                <option>Perempuan</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="jabatan_id" class="col-sm-2 col-form-label">Jabatan</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="jabatan_id" id="jabatan_id">
                                @foreach ($jabatans as $jabatan)
                                <option value="{{$jabatan->id}}">{{$jabatan->nama}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="bidang_id" class="col-sm-2 col-form-label">Bidang</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="bidang_id" id="bidang_id">
                                @foreach ($bidangs as $bidang)
                                <option value="{{$bidang->id}}">{{$bidang->nama}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="level_id" class="col-sm-2 col-form-label">Level</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="level_id" id="level_id">
                                @foreach ($levels as $level)
                                <option value="{{$level->id}}">{{$level->nama}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="email" class="col-sm-2 col-form-label">E-Mail</label>
                        <div class="col-sm-10">
                            <input type="email" class="form-control" name="email" id="email" placeholder="E-Mail">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="password" class="col-sm-2 col-form-label">Password</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                        </div>
                    </div>
                    <hr>
                    <div class="container" style="display: flex; justify-content: flex-end">
                        {{Form::submit('Simpan', ['class' => 'btn btn-primary', 'style' => 'height: 40px; margin-bottom:20px;'])}}
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</section>
@endsection
