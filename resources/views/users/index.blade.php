@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
@endsection

@section('content')
<section class="con-pad h-striped">
    <div class="container">
        <div style="display: flex; justify-content: flex-end; margin-bottom: 1rem">
            <a href="/users/create"><button class="btn btn-info"><i class="fa fa-plus" style="padding-right"></i> Tambah User</button></a>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3>Users</h3>
                    </div>
    
                    <div class="card-body">
                        <table class="table" id="datatable">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Nama</th>
                                    <th>NIP</th>
                                    <th>Jabatan</th>
                                    <th>Bidang</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $index => $user)
                                <tr>
                                    <td>{{$index +1}}</td>
                                    <td>{{$user->nama}}</td>
                                    <td>{{$user->NIP}}</td>
                                    <td>{{$user->jabatan->nama}}</td>
                                    <td>{{$user->bidang->nama}}</td>
                                    <td>
                                        <div class="row justify-content-around">

                                            <a type="button" href="/users/{{$user->id}}"><button class="btn btn-success"><i style="color: #fff;" class="fa fa-eye"></i></button></a>
                                            <a type="button" href="/users/{{$user->id}}/edit"><button class="btn btn-warning"><i style="color: #fff;" class="fa fa-pencil"></i></button></a>
                                            {!!Form::open(['action' => ['App\Http\Controllers\UsersController@destroy', $user->id], 'method' => 'POST'])!!}
                                                {{Form::hidden('_method', 'DELETE')}}
                                                <button type="submit" class="btn btn-danger"><i style="color: #fff;" class="fa fa-trash"></i></button>
                                            {!!Form::close()!!}
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('javascripts')
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready( function () {
            $('#datatable').DataTable({
                "language": {
                    "decimal":        "",
                    "emptyTable":     "Data tidak ditemukan",
                    "info":           "Menampilkan _START_ hingga _END_ dari _TOTAL_ data",
                    "infoEmpty":      "Menampilkan 0 hingga 0 dari 0 data",
                    "infoFiltered":   "(dari total _MAX_ data)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Menampilkan _MENU_ data",
                    "loadingRecords": "Loading...",
                    "processing":     "Processing...",
                    "search":         "Cari:",
                    "zeroRecords":    "Data tidak ditemukan",
                    "paginate": {
                        "next":       ">",
                        "previous":   "<"
                    },
                    "aria": {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    }
                }
            });
        });
    </script>
@endsection
