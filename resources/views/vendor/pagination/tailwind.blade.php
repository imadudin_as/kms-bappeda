@if ($paginator->hasPages())
    <nav role="navigation" aria-label="{{ __('Pagination Navigation') }}" class="col-12">
        <div class="pagination__number pull-right">
            @if ($paginator->onFirstPage())
                <span style="width: 110px; color: #777C81; background-color: #fff;">
                    {!! __('pagination.previous') !!}
                </span>
            @else
                <a href="{{ $paginator->previousPageUrl() }}" style="width: 110px">
                    {!! __('pagination.previous') !!}
                </a>
            @endif
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <a aria-disabled="true">
                        <a>{{ $element }}</a>
                    </a>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <span aria-current="page">
                                {{ $page }}
                            </span>
                        @else
                            <a href="{{ $url }}" aria-label="{{ __('Go to page :page', ['page' => $page]) }}">
                                {{ $page }}
                            </a>
                        @endif
                    @endforeach
                @endif
            @endforeach
            @if ($paginator->hasMorePages())
                <a href="{{ $paginator->nextPageUrl() }}" style="width: 110px">
                    {!! __('pagination.next') !!}
                </a>
            @else
                <span style="width: 110px; color: #777C81; background-color: #fff;">
                    {!! __('pagination.next') !!}
                </span>
            @endif
        </div>

        <div class="pull-left"">
            <div>
                <p class="text-sm text-gray-700 leading-5">
                    {!! __('Showing') !!}
                    <a class="font-medium">{{ $paginator->firstItem() }}</a>
                    {!! __('to') !!}
                    <a class="font-medium">{{ $paginator->lastItem() }}</a>
                    {!! __('of') !!}
                    <a class="font-medium">{{ $paginator->total() }}</a>
                    {!! __('results') !!}
                </p>
            </div>

            <div>
                <a class="relative z-0 inline-flex shadow-sm rounded-md">

                    {{-- Pagination Elements --}}
                    

                </a>
            </div>
        </div>
    </nav>
@endif
