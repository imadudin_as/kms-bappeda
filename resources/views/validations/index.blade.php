@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
@endsection

@section('content')
<section class="con-pad h-striped">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h3>Dokumen Memerlukan Validasi</h3>
                    </div>
    
                    <div class="card-body">
                        <table class="table" id="datatable">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Nama</th>
                                    <th>Author</th>
                                    <th>Tanggal</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($docs as $index => $doc)    
                                <tr>
                                    <td>{{$index +1}}</td>
                                    <td>{{$doc->nama}}</td>
                                    <td>{{$doc->user->nama}}</td>
                                    <td>{{ Carbon\Carbon::parse($doc->created_at)->format('d F Y')}}</td>
                                    <td>
                                        <a type="button" href="/validasi/{{$doc->id}}/edit"><button class="btn btn-success"><i style="color: #fff;" class="fa fa-eye"></i></button></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('javascripts')
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready( function () {
            $('#datatable').DataTable({
                "language": {
                    "decimal":        "",
                    "emptyTable":     "Tidak Ada Dokumen Memerlukan Validasi",
                    "info":           "Menampilkan _START_ hingga _END_ dari _TOTAL_ data",
                    "infoEmpty":      "Menampilkan 0 hingga 0 dari 0 data",
                    "infoFiltered":   "(dari total _MAX_ data)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Menampilkan _MENU_ data",
                    "loadingRecords": "Loading...",
                    "processing":     "Processing...",
                    "search":         "Cari:",
                    "zeroRecords":    "Dokumen Tidak Ditemukan",
                    "paginate": {
                        "next":       ">",
                        "previous":   "<"
                    },
                    "aria": {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    }
                }
            });
        });
    </script>
@endsection
