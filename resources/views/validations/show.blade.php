@extends('layouts.app')


@section('content')
<section class="con-pad h-striped">
    <div class="container">
        <div class="card" style="padding: 1rem 0rem">
            <div class="card-body row">
                <div class="col-3">
                    <a href="#" onclick="history.go(-1)"><button class="btn btn-secondary"><i class="fa fa-chevron-left" style="padding-right: 15px"></i> Kembali</button></a>
                </div>
            </div>
            <hr>
            <div class="col-12 row">
                <div class="col-md-3 align-self-center">
                    <img src="{{ asset('img') }}/png/pdf.png" alt="">
                </div>
                <div class="col-md-9" style="border-left: 0.5px solid">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-sm-3">
                                    Nama Dokumen
                                </div>
                                <div class="col-auto">
                                    :
                                </div>
                                <div class="col-sm-8">
                                    Dokumen Pertama
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-sm-3">
                                    Author
                                </div>
                                <div class="col-auto">
                                    :
                                </div>
                                <div class="col-sm-8">
                                    Imadudin Abdan Syakuro
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-sm-3">
                                    Tanggal
                                </div>
                                <div class="col-auto">
                                    :
                                </div>
                                <div class="col-sm-8">
                                    10 November 2020
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-sm-3">
                                    Halaman
                                </div>
                                <div class="col-auto">
                                    :
                                </div>
                                <div class="col-sm-8">
                                    20 Halaman
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-sm-3">
                                    Deskripsi
                                </div>
                                <div class="col-auto">
                                    :
                                </div>
                                <div class="col-sm-8">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut natus exercitationem voluptatibus, aperiam eos perspiciatis consequatur minus officiis pariatur fuga quae ex est! Ea ab accusamus ut ipsam distinctio. Cum.
                                    Eveniet rem ipsam necessitatibus, vel inventore aliquid quibusdam ex iure, nulla aperiam nobis eius placeat tempore, libero fuga deleniti labore veritatis assumenda autem nam id sit laboriosam! Nemo, aliquam facere.
                                    Illo, quibusdam aut voluptates doloribus vel, deserunt deleniti facere soluta, a numquam quam fugit enim quo nam in maxime nihil iusto saepe eligendi? Cumque assumenda aspernatur alias tempore! Esse, debitis?
                                    Facere fugiat dolores voluptate, nulla nisi corrupti aperiam recusandae. Magnam placeat expedita aperiam a consectetur. Minus quibusdam ipsa sequi et magni laudantium, placeat aspernatur illum natus nesciunt temporibus sint excepturi.
                                    Velit, earum. Nemo, facere voluptas quasi odio aspernatur officiis iusto beatae dignissimos, illo quos laboriosam animi ad accusamus quo! Nulla ea autem, possimus distinctio ut illum excepturi vero! Aut, ut.
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-sm-3">
                                    Validasi
                                </div>
                                <div class="col-auto">
                                    :
                                </div>
                                <div class="col-sm-8">
                                    Belum Divalidasi
                                </div>
                            </div>
                        </li>
                    </ul>
                    <br>
                    <div class="row justify-content-end">
                        <a href="/validasi/1/edit"><button class="btn btn-warning"> Validasi <i class="fa fa-caret-right" style="padding-left: 15px"></i></button></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
