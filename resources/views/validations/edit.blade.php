@extends('layouts.app')


@section('content')
<section class="con-pad h-striped">
    <div class="container">
        <div class="card" style="padding: 1rem 0rem">
            <div class="card-body row">
                <div class="col-3">
                    <a href="#" onclick="history.go(-1)"><button class="btn btn-secondary"><i class="fa fa-chevron-left" style="padding-right: 15px"></i> Kembali</button></a>
                </div>
            </div>
            <hr>
            <div class="col-12 row">
                <div class="col-md-3 align-self-center">
                    <img src="{{ asset('img') }}/png/{{$doc->jenis_file}}.png" alt="">
                    <div style="display: flex; justify-content:center; margin-top: 20px;">
                        <a href="/storage/file/{{$doc->file}}" download=""><button class="btn btn-primary"><i class="fa fa-download" style="padding-right: 15px"></i> Download File</button></a>
                    </div>
                </div>
                <div class="col-md-9" style="border-left: 0.5px solid">
                    {!! Form::open(['action' => ['App\Http\Controllers\ValidationsController@update', $doc->id], 'method' => 'POST']) !!}
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-sm-3">
                                        Nama Dokumen
                                    </div>
                                    <div class="col-auto">
                                        :
                                    </div>
                                    <div class="col-sm-8">
                                        {{$doc->nama}}
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-sm-3">
                                        Author
                                    </div>
                                    <div class="col-auto">
                                        :
                                    </div>
                                    <div class="col-sm-8">
                                        {{$doc->user->nama}}
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-sm-3">
                                        Diupload Tanggal
                                    </div>
                                    <div class="col-auto">
                                        :
                                    </div>
                                    <div class="col-sm-8">
                                        {{ Carbon\Carbon::parse($doc->tgl_upload)->format('d F Y')}}
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-sm-3">
                                        Halaman
                                    </div>
                                    <div class="col-auto">
                                        :
                                    </div>
                                    <div class="col-sm-8">
                                        {{$doc->halaman}} Halaman
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-sm-3">
                                        Tahun
                                    </div>
                                    <div class="col-auto">
                                        :
                                    </div>
                                    <div class="col-sm-8">
                                        {{$doc->tahun}}
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-sm-3">
                                        Deskripsi
                                    </div>
                                    <div class="col-auto">
                                        :
                                    </div>
                                    <div class="col-sm-8">
                                        {{$doc->deskripsi}}    
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="form-group row">
                                    <div class="col-sm-3 align-self-center">
                                        Validasi
                                    </div>
                                    <div class="col-auto align-self-center">
                                        :
                                    </div>
                                    <div class="col-sm-8">
                                        <select name="status_id" id="status_id" class="form-control">
                                            @foreach ($statuses as $status)
                                            <option value="{{$status->id}}">{{$status->nama}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </li>
                            {{Form::hidden('_method', 'PUT')}}
                            <li class="list-group-item">
                                <div class="row justify-content-end">
                                    {{Form::submit('Validasi', ['class' => 'btn btn-warning'])}}
                                </div>
                                {{-- <a href="/validasi/1/edit"><button class="btn btn-warning"> Validasi <i class="fa fa-caret-right" style="padding-left: 15px"></i></button></a> --}}
                            </li>
                        </ul>
                        {!! Form::close() !!}
                    <br>
                    <div class="row justify-content-end">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
