<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/index');
});

// Route::get('/forum', 'App\Http\Controllers\PagesController@forum_diskusi');
Route::get('/index', 'App\Http\Controllers\PagesController@index');
// Route::get('/user', 'App\Http\Controllers\PagesController@manage_user');
// Route::get('/validasi', 'App\Http\Controllers\PagesController@validasi_dokumen');

// Route Docs
Route::post('comment_create', [
    'uses' => 'App\Http\Controllers\DocumentsController@comment_create'
  ]);
Route::post('versi_create', [
    'uses' => 'App\Http\Controllers\DocumentsController@versi_create'
]);
Route::get('/docs/edit_komen/{id}', [
    'uses' => 'App\Http\Controllers\DocumentsController@comment_edit'
  ]);
Route::post('/docs/edit_komen/{id}', [
    'uses' => 'App\Http\Controllers\DocumentsController@comment_update'
  ]);
Route::post('/docs/delete_komen/{id}', [
    'uses' => 'App\Http\Controllers\DocumentsController@comment_delete'
  ]);
Route::get('/docs/delete_komen/{id}', [
    'uses' => 'App\Http\Controllers\DocumentsController@comment_delete'
  ]);
Route::get('/docs/search', 'App\Http\Controllers\DocumentsController@search');
Route::resource('/docs', 'App\Http\Controllers\DocumentsController');


// Route Request
Route::post('req_comment_create', [
  'uses' => 'App\Http\Controllers\RequestsController@req_comment_create'
]);
Route::get('/request/edit_komen/{id}', [
  'uses' => 'App\Http\Controllers\RequestsController@req_comment_edit'
]);
Route::post('/request/edit_komen/{id}', [
  'uses' => 'App\Http\Controllers\RequestsController@req_comment_update'
]);
Route::post('/request/delete_komen/{id}', [
  'uses' => 'App\Http\Controllers\RequestsController@req_comment_delete'
]);
Route::get('/request/delete_komen/{id}', [
  'uses' => 'App\Http\Controllers\RequestsController@req_comment_delete'
]);
Route::get('selesai', 'App\Http\Controllers\RequestsController@selesai');
Route::resource('/request', 'App\Http\Controllers\RequestsController');

// Route Users
Route::resource('/users', 'App\Http\Controllers\UsersController');

// Route Validasi
Route::resource('/validasi', 'App\Http\Controllers\ValidationsController');

// Route Forum
Route::post('diskusi_create', [
  'uses' => 'App\Http\Controllers\ForumsController@diskusi_create'
]);
Route::get('tutup', 'App\Http\Controllers\ForumsController@tutup');
Route::get('buka', 'App\Http\Controllers\ForumsController@buka');
Route::resource('/forum', 'App\Http\Controllers\ForumsController');

// Route Profil
Route::resource('/profil', 'App\Http\Controllers\ProfilsController');

Auth::routes();

Route::get('/index', [App\Http\Controllers\HomeController::class, 'index'])->name('index');
Route::get('/dashboard', [App\Http\Controllers\DashboardController::class, 'index'])->name('dashboard');
